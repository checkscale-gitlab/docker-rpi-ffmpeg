#
# FFMPEG on the Raspberry Pi
#
# BUILD:
# docker build -t fradelg/ffmpeg .
#
# TEST:
# docker run --rm -it --privileged --device /dev/video0:/dev/video0 fradelg/rpi-ffmpeg ffmpeg -f v4l2 -framerate 25 -video_size 640x480 -i /dev/video0 output.mkv
#

FROM resin/rpi-raspbian:jessie
LABEL version="3.2.5" maintainer="Fco. Javier Delgado del Hoyo <frandelhoyo@gmail.com>"

# Update packages, install dependencies and clean cache
RUN apt-get -y update && apt-get -y upgrade && \
    apt-get -y install curl build-essential nasm libx264-dev libv4l-dev && \
    apt-get -y autoremove && apt-get -y clean && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/local/src

# Compile FFmpeg from source with x264 support
RUN curl -L https://github.com/FFmpeg/FFmpeg/archive/n3.2.5.tar.gz | tar xz && \
    cd FFmpeg-n3.2.5 && \
    ./configure --enable-gpl --enable-shared --enable-libx264 && \
    make && make install && ldconfig && \
    rm -rf /usr/local/src/*

CMD ["/bin/bash"]
