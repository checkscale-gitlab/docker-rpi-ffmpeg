for DIR in */
do
  VERSION=`cat ${DIR}/Dockerfile | grep version | sed "s/^.*version=\"\([^\"]*\).*/\1/"`
  docker image build -t $DOCKER_IMAGE:$VERSION $DIR
  docker image push $DOCKER_IMAGE:$VERSION
done

docker image tag $DOCKER_IMAGE:$VERSION $DOCKER_IMAGE:latest
docker image push $DOCKER_IMAGE:latest
